IMAGE_NAME ?= $(shell git remote get-url --push origin | awk -F'/|.git' '{print $$3}')
IMAGE_VERSION = 0.0.1

echo:
	echo "Building $(IMAGE_NAME) $(IMAGE_VERSION)"

local:
	gin -p 8081 run main.go

build: main.go
	echo "Building $(IMAGE_NAME) $(IMAGE_VERSION)"
	env $(cat .credentials | xargs ) \
	   docker build \
		   --build-arg IMAGE_NAME=$(IMAGE_NAME) \
		   --build-arg IMAGE_VERSION=$(IMAGE_VERSION) \
		   --build-arg GIT_USER \
		   --build-arg GIT_PASS \
		   -t caca .

.PHONY = test
test: 
	go test
