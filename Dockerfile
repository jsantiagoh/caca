# Multistage build for the application
# Builder image
FROM golang:1.10 as builder

ARG GIT_USER=$GIT_USER
ARG GIT_PASS=$GIT_PASS

RUN echo "machine bitbucket.org\n\tlogin $GIT_USER\n\tpassword $GIT_PASS" >> ~/.netrc

RUN go get github.com/golang/dep/cmd/dep

WORKDIR /go/src/gitlab.com/jsantiagoh/caca
COPY . .

RUN dep ensure -v -vendor-only

RUN make test

RUN CGO_ENABLED=0 GOOS=linux go build -v -o /out/caca

# -------------------------------------
# Final Image
FROM scratch

WORKDIR /src/app

ARG IMAGE_VERSION
ARG IMAGE_NAME

LABEL VERSION=${IMAGE_VERSION}
LABEL NAME=${IMAGE_NAME}

# Add Root CA certificates to scratch
COPY --from=builder /etc/ssl/certs/ca-certificates.crt  /etc/ssl/certs/ca-certificates.crt


# Add the binary
COPY --from=builder out/ .

EXPOSE 8080

CMD ["./caca"]

