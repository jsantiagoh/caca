package main

import (
	"context"
	"encoding/json"
	"expvar"
	"fmt"
	"net/http"
	"os"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/metrics"
	kitexpvar "github.com/go-kit/kit/metrics/expvar"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"

	privlib "bitbucket.org/santiago_lebara/golib"
)

const (
	requestCountName = "request.something.count"
)

// Service is the main service of the app
type Service interface {
	// Something is one of the business logic methods
	Something(context.Context) string
}

type service struct {
}

// Something is a concrete impementation of the business method
func (s service) Something(ctx context.Context) string {
	return privlib.Secret()
}

func newService() Service {
	return service{}

}

func main() {

	var svc Service
	svc = newService()

	requestCount := kitexpvar.NewCounter(requestCountName)

	svc = instrumentingMiddleware{requestCount, svc}

	r := mux.NewRouter()
	r.Methods("GET").Path("/").Handler(makeHandler(svc))
	r.Methods("GET").Path("/exp").Handler(expvar.Handler())

	addr := ":8080"
	if port, ok := os.LookupEnv("PORT"); ok {
		addr = fmt.Sprintf(":%s", port)
	}
	log.WithField("addr", addr).Info("Listening...")
	_ = http.ListenAndServe(addr, r)
}

func makeHandler(svc Service) http.Handler {
	return handlers.RecoveryHandler()(
		kithttp.NewServer(
			makeEndpoint(svc),
			decodeRequest,
			encodeResponse,
		))
}

func makeEndpoint(svc Service) endpoint.Endpoint {
	return func(ctx context.Context, _ interface{}) (interface{}, error) {
		return response{svc.Something(ctx)}, nil
	}
}

type response struct {
	Secret string `json:"secret,omitempty"`
}

func decodeRequest(_ context.Context, _ *http.Request) (interface{}, error) {
	return nil, nil
}

func encodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	return json.NewEncoder(w).Encode(response)
}

// instrumentingMiddleware implements Service and also adds counters
type instrumentingMiddleware struct {
	requestCount metrics.Counter
	next         Service
}

func (mw instrumentingMiddleware) Something(ctx context.Context) string {
	defer func(begin time.Time) {
		mw.requestCount.With("value").Add(1)
	}(time.Now())

	return mw.next.Something(ctx)
}
