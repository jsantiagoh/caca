# This is caca

An experiment project on building go applications that depend on private repos

Basically this is what's needed

Create an app token in [Application Passwords](https://bitbucket.org/account/user/santiago_lebara/app-passwords)

Add the dependency using `dep ensure -add  "bitbucket.org/santiago_lebara/golib@^0.0.3"`

Keep in mind that locally `dep ensure` will try to use _HTTPS_ and then _SSH_ for downloading the dependencies, if the private keys are already set for _SSH_ locally, there's nothing needed. 
When building inside the container, there's no access to the _SSH_ private key, so can fall back to use _HTTPS_ with username and the application token generated for this.

write down a `.credentials` file with the following:
```
GIT_USER=username
GIT_PASS=[application password]
```

On Jenkins define the environment variables and there's no need for `.credentials`